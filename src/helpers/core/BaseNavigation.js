import React from 'react';
import {createSwitchNavigator} from 'react-navigation';

import AppNavigation from '../AppNavigation';

export default createSwitchNavigator({
    Main: AppNavigation,
});