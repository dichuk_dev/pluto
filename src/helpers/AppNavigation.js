import React from 'react';
import {createStackNavigator} from "react-navigation";

import AuthScreen from '../screens/auth/AuthScreen';
import EmailConfirmationScreen from "../screens/auth/EmailConfirmationScreen";
import DashboardScreen from "../screens/dashboard/DashboardScreen";
import HelpScreen from "../screens/dashboard/HelpScreen";

const App = createStackNavigator(
    {
        AuthScreen: AuthScreen,
        EmailConfirmationScreen: EmailConfirmationScreen,
        DashboardScreen: DashboardScreen,
        HelpScreen: HelpScreen
    }
);

export default App;