import React from 'react'
import {StyleSheet, View} from "react-native";
import BaseScreen from "../core/BaseScreen";
import {RenderButton} from "../../components/button";

export default class DashboardScreen extends BaseScreen {
    static navigationOptions = {
        title: "Homepage",
        headerTintColor: '#ffffff',
        headerStyle: {
            fontSize: 14,
            backgroundColor: '#074445',
        },
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{flex: 1, bottom: 0}}>
                    <RenderButton
                        label={'Help'}
                        marginLR={20}
                        textColor={'#ffffff'}
                        background={'#074445'}
                        onPress={() => {
                            this.props.navigation.navigate('HelpScreen')
                        }}
                    />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eeece6',
    }
});