import React from 'react'
import {StyleSheet, View} from "react-native";
import BaseScreen from "../core/BaseScreen";
import Intercom from 'react-native-intercom';

export default class HelpScreen extends BaseScreen {
    static navigationOptions = {
        title: "Help",
        headerTintColor: '#ffffff',
        headerStyle: {
            fontSize: 14,
            backgroundColor: '#074445',
        },
    };

    render() {
        Intercom.registerIdentifiedUser({userId: 'Bob'});
        Intercom.logEvent('viewed_screen', {extra: 'metadata'});
        return (
            <View style={styles.container}>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eeece6',
    }
});