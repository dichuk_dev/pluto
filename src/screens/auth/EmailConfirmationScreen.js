import React from 'react'
import {StyleSheet, View} from "react-native";
import BaseScreen from "../core/BaseScreen";
import {RenderButton} from "../../components/button";
import {Text} from "react-native-elements";

export default class EmailConfirmationScreen extends BaseScreen {

    static navigationOptions = {
        title: "Email confirmation",
        headerTintColor: '#ffffff',
        headerStyle: {
            fontSize: 14,
            backgroundColor: '#074445',
        },
    };

    constructor(props) {
        super(props);
        this.state = {email: ''}
    }


    render() {
        const email = this.state.email;
        return (
            <View style={styles.container}>
                <View style={{flex: 10}}>
                    <Text style={styles.headerText}>We have send a{'\n'}code to{'\n'}{email}</Text>
                </View>
                <View style={{flex: 1}}>
                    <RenderButton
                        label={'Login'}
                        marginLR={20}
                        textColor={'#ffffff'}
                        background={'#074445'}
                        onPress={() => {
                            this.props.navigation.navigate('DashboardScreen')
                        }}
                    />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eeece6',
    },
    headerText: {
        color: '#000000',
        marginTop: 23,
        textAlign: 'center',
        fontSize: 24,
    },
});