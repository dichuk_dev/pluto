import React from 'react'
import {default as Alert, StyleSheet, ToastAndroid, View} from "react-native";
import BaseScreen from "../core/BaseScreen";
import {RenderButton} from "../../components/button";
import Auth0 from 'react-native-auth0';

const credentials = require('./auth0-credentials');
const auth0 = new Auth0(credentials);

export default class AuthScreen extends BaseScreen {

    static navigationOptions = {
        title: "Login",
        headerTintColor: '#ffffff',
        headerStyle: {
            fontSize: 14,
            backgroundColor: '#074445',
        },
    };

    constructor(props) {
        super(props);
        this.state = {email: ''}
    }

    _onLogin = () => {
        auth0.webAuth
            .authorize({
                scope: 'openid profile email',
                audience: 'https://' + credentials.domain + '/userinfo'
            })
            .then(credentials => {
                Alert.alert(
                    'Success',
                    'AccessToken: ' + credentials.accessToken, [{
                        text: 'OK',
                        onPress: () => {
                        }
                    }], {cancelable: false}
                );
                {
                    this.setState({accessToken: credentials.accessToken});
                    this.props.navigation.navigate('DashboardScreen');
                }
            })
            .catch(error => {
                console.log('error');
                ToastAndroid.show("show  " + error.message, ToastAndroid.SHORT);
            });
    };

    render() {
        return (
            <View style={styles.container}>
                {/*<View style={{flex: 1, marginTop: 46}}>*/}
                {/*    <RenderInputText*/}
                {/*        // onChangeText={(email) => this.setState(email)}*/}
                {/*        value={this.state.email}*/}
                {/*        label={'Email'}*/}
                {/*    />*/}
                {/*</View>*/}
                <View style={{flex: 1, marginTop: 30}}>
                    <RenderButton
                        label={'Login'}
                        marginLR={20}
                        textColor={'#eeece6'}
                        background={'#074445'}
                        onPress={() => {
                            // this._onLogin();
                            this.props.navigation.navigate('HelpScreen');
                        }}
                    />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eeece6',
    },

});

