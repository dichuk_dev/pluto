import React from "react";
import {StyleSheet} from "react-native";
import {TextInput} from "react-native-paper";

export const RenderInputText = ({label, onChangeText}) => (
    <TextInput
        label={label}
        style={[styles.inputContainerStyle, styles.textArea]}
        mode="outlined"
        dense
        theme={{
            colors: {
                placeholder: '#7c8184', text: '#000000', primary: '#074445',
            }
        }}
        onChangeText={onChangeText}/>
);
const styles = StyleSheet.create({
    inputContainerStyle: {
        margin: 10,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#eeece6',
    },
    textArea: {
        height: 72,
        color: '#000000',
        fontSize: 16
    },
});