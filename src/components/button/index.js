import React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

export const RenderButton = ({label, textColor, background, onPress, marginLR}) => (
    <View rounded style={StyleSheet.flatten([styles.section, styles.btnSection, {
        marginLeft: marginLR,
        marginRight: marginLR
    }])}>
        <TouchableOpacity
            style={StyleSheet.flatten([styles.button, {
                backgroundColor: background,
            }])}
            onPress={onPress}>
            <Text uppercase={true}
                  style={StyleSheet.flatten([styles.buttonText, {color: textColor}])}>{label}</Text>
        </TouchableOpacity>
    </View>
);
const styles = StyleSheet.create({
    section: {
        marginTop: 0
    },
    btnSection: {
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '100%',
        height: 46,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 1
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        paddingTop: 3,
        lineHeight: 16
    },
});