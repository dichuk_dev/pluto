import React, {Component} from 'react';
import AppNavigation from "./src/helpers/AppNavigation";

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <AppNavigation/>
        );
    }
}
